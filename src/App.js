import React from 'react';
import { HashRouter as Router, Route, Switch, Link } from 'react-router-dom';

import Home from './components/Home';
import Other from './components/Other';

const App = () => (
  <Router>
    <div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/other">Other</Link>
        </li>
      </ul>
      <hr />
      <Switch>
        <Route path="/other" component={Other} />
        <Route path="/" component={Home} />
      </Switch>
    </div>
  </Router>
);

export default App;

