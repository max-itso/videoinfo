import React from 'react';

const Other = () => (
  <div>
    <h1 className="title has-text-danger">Other page</h1>
    <h2 className="subtitle">Looks like it is empty... :/</h2>
  </div>
);


export default Other;
