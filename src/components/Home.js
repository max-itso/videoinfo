import React from 'react';

const Home = () => (
  <div>
    <h1 className="title has-text-primary">Hello World!</h1>
    <h2 className="subtitle">What is it you are building ?</h2>
  </div>
);

export default Home;
